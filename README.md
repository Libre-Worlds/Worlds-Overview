# Worlds-Overview

An overview of the (intended and existing) Libre-Worlds Universes, Realms and Historical settings.

In addition, a view of further known settings based on libre material is provided.


Libre-Worlds
----------------------

 * Earth'n Mars Chronicles (Sci-Fi)
 * ManaPunk / MagePunk / Steam-o'Punk (Fantasy)
 * Siege and Succession (Historical - ie primarily Trademark)

 Other Settings based on Libre material
 ------------------------------------------